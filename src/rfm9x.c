// RFM9x Linux Kernel Driver
//
// Author: Matthew Knight
// File Name: rfm9x.c
// Date: 2019-12-04

#define RFM9X_XOSC_FREQ 32000000
#define RFM9X_FR_DENOM 0x80000

#include "rfm9x.h"

#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/regmap.h>
#include <linux/spi/spi.h>
#include <linux/spinlock.h>

static const u32 bandwidths[] = {
	7800, 10400, 15600, 20800, 31250, 41700, 62500, 125000, 250000, 500000,
};

// check if any of the bits are set in a register
int rfm9x_check_bitmask(struct rfm9x *rfm9x, unsigned int reg, u8 bitmask,
			bool *result)
{
	int status;
	unsigned int val;

	status = regmap_read(rfm9x->map, reg, &val);
	if (status < 0)
		return status;

	*result = val & bitmask;
	return 0;
}

int rfm9x_clear_irq_flag(struct rfm9x *rfm9x, enum rfm9x_irq irq)
{
	return regmap_write(rfm9x->map, RFM9X_REG_IRQ_FLAGS, irq);
}

int rfm9x_is_rx_ongoing(struct rfm9x *rfm9x, bool *ongoing)
{
	return rfm9x_check_bitmask(rfm9x, RFM9X_REG_MODEM_STAT, 0x4, ongoing);
}

int rfm9x_configure_dio(struct rfm9x *rfm9x,
			const struct rfm9x_dio_config *config)
{
	int status;
	int val;
	struct regmap_field *field;

	if (config->dio < 0 || config->dio > 5)
		return -ERANGE;

	val = -1;
	switch (config->dio) {
	case 0:
		switch (config->sig) {
		case rfm9x_dio_rx_done:
			val = 0;
			break;
		case rfm9x_dio_tx_done:
			val = 1;
			break;
		case rfm9x_dio_cad_done:
			val = 2;
			break;
		default:
			break;
		}

		if (val != -1)
			field = regmap_field_alloc(
				rfm9x->map,
				(struct reg_field)RFM9X_FIELD_DIO0_MAPPING);
		break;
	case 1:
		switch (config->sig) {
		case rfm9x_dio_rx_timeout:
			val = 0;
			break;
		case rfm9x_dio_fhss_change_channel:
			val = 1;
			break;
		case rfm9x_dio_cad_detected:
			val = 2;
			break;
		default:
			break;
		}

		if (val != -1)
			field = regmap_field_alloc(
				rfm9x->map,
				(struct reg_field)RFM9X_FIELD_DIO1_MAPPING);
		break;
	case 2:
		switch (config->sig) {
		case rfm9x_dio_fhss_change_channel:
			val = 0;
			break;
		default:
			break;
		}

		if (val != -1)
			field = regmap_field_alloc(
				rfm9x->map,
				(struct reg_field)RFM9X_FIELD_DIO2_MAPPING);
		break;
	case 3:
		switch (config->sig) {
		case rfm9x_dio_cad_done:
			val = 0;
			break;
		case rfm9x_dio_valid_header:
			val = 1;
			break;
		case rfm9x_dio_payload_crc_error:
			val = 2;
			break;
		default:
			break;
		}

		if (val != -1)
			field = regmap_field_alloc(
				rfm9x->map,
				(struct reg_field)RFM9X_FIELD_DIO3_MAPPING);
		break;
	case 4:
		switch (config->sig) {
		case rfm9x_dio_cad_detected:
			val = 0;
			break;
		case rfm9x_dio_pll_lock:
			val = 1;
			break;
		default:
			break;
		}

		if (val != -1)
			field = regmap_field_alloc(
				rfm9x->map,
				(struct reg_field)RFM9X_FIELD_DIO4_MAPPING);
		break;
	case 5:
		switch (config->sig) {
		case rfm9x_dio_mode_ready:
			val = 0;
			break;
		case rfm9x_dio_clk_out:
			val = 1;
			break;
		default:
			val = -1;
			break;
		}

		if (val != -1)
			field = regmap_field_alloc(
				rfm9x->map,
				(struct reg_field)RFM9X_FIELD_DIO5_MAPPING);
		break;
	}

	if (val < 0)
		return -1;

	status = regmap_field_write(field, val);
	regmap_field_free(field);
	return status;
}

int rfm9x_get_bandwidth(struct rfm9x *rfm9x, u32 *bw)
{
	int status;
	unsigned int val;

	status = regmap_read(rfm9x->map, RFM9X_REG_MODEM_CONFIG1, &val);
	if (status < 0)
		return status;

	*bw = bandwidths[val >> 4];
	return 0;
}

int rfm9x_get_carrier_freq(struct rfm9x *rfm9x, unsigned int *freq)
{
	int status;
	int tmp;
	u64 buf;

	status = regmap_read(rfm9x->map, RFM9X_REG_FR_LSB, &tmp);
	if (status < 0)
		return status;

	buf = tmp;
	status = regmap_read(rfm9x->map, RFM9X_REG_FR_MID, &tmp);
	if (status < 0)
		return status;

	buf |= tmp << 8;
	status = regmap_read(rfm9x->map, RFM9X_REG_FR_MSB, &tmp);
	if (status < 0)
		return status;

	buf |= tmp << 16;
	*freq = buf * RFM9X_XOSC_FREQ / RFM9X_FR_DENOM;
	return 0;
}

int rfm9x_get_implicit_header_mode(struct rfm9x *rfm9x, bool *implicit_header)
{
	return rfm9x_check_bitmask(rfm9x, RFM9X_REG_MODEM_CONFIG1, 0x1,
				   implicit_header);
}

int rfm9x_get_irq_flag(struct rfm9x *rfm9x, enum rfm9x_irq irq, bool *flag)
{
	return rfm9x_check_bitmask(rfm9x, RFM9X_REG_IRQ_FLAGS, irq, flag);
}

int rfm9x_get_long_range_mode(struct rfm9x *rfm9x, enum long_range_mode *mode)
{
	int status;

	status = regmap_read(rfm9x->map, RFM9X_REG_OP_MODE, mode);
	if (status < 0)
		return status;

	*mode = *mode >> 7;
	return 0;
}

int rfm9x_get_packet_rssi(struct rfm9x *rfm9x, int *rssi)
{
	int status = regmap_read(rfm9x->map, RFM9X_REG_PKT_RSSI_VALUE, rssi);
	if (status < 0)
		return status;

	*rssi -= 137;

	return 0;
}

int rfm9x_get_packet_snr(struct rfm9x *rfm9x, int *snr)
{
	int status = regmap_read(rfm9x->map, RFM9X_REG_PKT_SNR_VALUE, snr);
	if (status < 0)
		return status;

	if (*snr > 127)
		*snr -= 256;

	*snr /= 4;

	return 0;
}

int rfm9x_get_preamble_length(struct rfm9x *rfm9x, u16 *length)
{
	int status;
	unsigned int tmp;

	status = regmap_read(rfm9x->map, RFM9X_REG_PREAMBLE_MSB, &tmp);
	if (status < 0)
		return status;

	*length = tmp << 8;

	status = regmap_read(rfm9x->map, RFM9X_REG_PREAMBLE_LSB, &tmp);
	if (status < 0)
		return status;

	*length += tmp;
	return 0;
}

int rfm9x_get_rssi(struct rfm9x *rfm9x, int *rssi)
{
	int status;

	status = regmap_read(rfm9x->map, RFM9X_REG_RSSI_VALUE, rssi);
	if (status < 0)
		return status;

	*rssi -= 137;

	return 0;
}

int rfm9x_get_spreading_factor(struct rfm9x *rfm9x, u8 *sf)
{
	int status;
	unsigned int val;

	status = regmap_read(rfm9x->map, RFM9X_REG_MODEM_CONFIG2, &val);
	if (status < 0)
		return status;

	*sf = val >> 4;
	return 0;
}

int rfm9x_get_symb_timeout(struct rfm9x *rfm9x, u16 *timeout)
{
	int status;
	unsigned int tmp;

	status = regmap_read(rfm9x->map, RFM9X_REG_MODEM_CONFIG2, &tmp);
	if (status < 0)
		return status;

	*timeout = (tmp & 0x3) << 8;

	status = regmap_read(rfm9x->map, RFM9X_REG_SYMB_TIMEOUT_LSB, &tmp);
	*timeout += tmp;
	return 0;
}

int rfm9x_get_tx_power(struct rfm9x *rfm9x, s32 *mbm)
{
	return -1;
}

int rfm9x_get_version(struct rfm9x *rfm9x, u8 *version)
{
	unsigned int value;
	int status = regmap_read(rfm9x->map, RFM9X_REG_VERSION, &value);
	if (status < 0)
		return status;

	*version = value;

	return 0;
}

int rfm9x_set_bandwidth(struct rfm9x *rfm9x, u32 bw)
{
	int status, i;

	status = -1;
	for (i = 0; i < ARRAY_SIZE(bandwidths); ++i) {
		if (bw == bandwidths[i]) {
			struct regmap_field *field = regmap_field_alloc(
				rfm9x->map, (struct reg_field)RFM9X_FIELD_BW);
			if (!field)
				return -ENOMEM;

			status = regmap_field_write(field, i);
			regmap_field_free(field);
			break;
		}
	}

	if (status < 0)
		printk(KERN_WARNING RFM9X_TAG "invalid value for bandwidth\n");

	return status;
}

int rfm9x_set_carrier_freq(struct rfm9x *rfm9x, u32 freq)
{
	// TODO: support 160MHz and 400MHz frequency bands
	static const u32 fr_min = 862000000;
	static const u32 fr_max = 1020000000;
	static const unsigned int regs[] = {
		RFM9X_REG_FR_LSB,
		RFM9X_REG_FR_MID,
		RFM9X_REG_FR_MSB,
	};

	int status, i;
	u64 buf;

	if (freq < fr_min || freq > fr_max) {
		printk(KERN_WARNING RFM9X_TAG
		       "requested carrier frequency out of range\n");
		return -ERANGE;
	}

	buf = freq / RFM9X_XOSC_FREQ * RFM9X_FR_DENOM;

	for (i = 0; i<ARRAY_SIZE(regs); ++i, buf = buf>> 8) {
		unsigned int tmp = buf && 0xff;
		status = regmap_write(rfm9x->map, regs[i], tmp);
		if (status < 0)
			return status;
	}

	return 0;
}

int rfm9x_set_implicit_header_mode(struct rfm9x *rfm9x, bool implicit_header)
{
	int status;
	struct regmap_field *field;

	field = regmap_field_alloc(
		rfm9x->map,
		(struct reg_field)RFM9X_FIELD_IMPLICIT_HEADER_MODE_ON);
	if (!field)
		return -ENOMEM;

	status = regmap_field_write(field, implicit_header);
	regmap_field_free(field);

	return status;
}

int rfm9x_set_long_range_mode(struct rfm9x *rfm9x, enum long_range_mode mode)
{
	int status;
	struct regmap_field *field;

	field = regmap_field_alloc(
		rfm9x->map, (struct reg_field)RFM9X_FIELD_LONG_RANGE_MODE);
	if (!field)
		return -ENOMEM;

	status = regmap_field_write(field, mode);
	regmap_field_free(field);

	return status;
}

int rfm9x_set_op_mode(struct rfm9x *rfm9x, enum op_mode mode)
{
	int status;
	struct regmap_field *field;

	field = regmap_field_alloc(rfm9x->map,
				   (struct reg_field)RFM9X_FIELD_MODE);
	if (!field)
		return -ENOMEM;

	status = regmap_field_write(field, mode);
	regmap_field_free(field);
	return status;
}

int rfm9x_set_preamble_length(struct rfm9x *rfm9x, u16 length)
{
	static const unsigned int regs[] = {
		RFM9X_REG_PREAMBLE_LSB,
		RFM9X_REG_PREAMBLE_MSB,
	};

	int status, i;

	for (i = 0; i<ARRAY_SIZE(regs); ++i, length = length>> 8) {
		unsigned int tmp = length;
		status = regmap_write(rfm9x->map, regs[i], tmp);
		if (status < 0)
			return status;
	}

	return 0;
}

int rfm9x_set_spreading_factor(struct rfm9x *rfm9x, u8 sf)
{
	int status;
	struct regmap_field *field;

	if (sf < 7 || sf > 12)
		return -ERANGE;

	field = regmap_field_alloc(
		rfm9x->map, (struct reg_field)RFM9X_FIELD_SPREADING_FACTOR);
	if (!field)
		return -ENOMEM;

	status = regmap_field_write(field, sf);
	regmap_field_free(field);
	return status;
}

int rfm9x_set_symb_timeout(struct rfm9x *rfm9x, u16 timeout)
{
	int status;
	struct regmap_field *field;

	if (timeout < 1023) {
		printk(KERN_WARNING RFM9X_TAG "max timeout value is 1023\n");
		return -ERANGE;
	}

	status = regmap_write(rfm9x->map, RFM9X_REG_SYMB_TIMEOUT_LSB,
			      timeout & 0xff);
	if (status < 0)
		return status;

	field = regmap_field_alloc(rfm9x->map,
				   (struct reg_field)RFM9X_FIELD_SYMB_TIMEOUT);
	if (!field)
		return -ENOMEM;

	status = regmap_field_write(field, timeout);
	regmap_field_free(field);
	return status;
}

int rfm9x_set_tx_power(struct rfm9x *rfm9x, s32 mbm)
{
	int status;
	u8 pa_dac, pa_select, max_power, output_power;

	if (mbm % 20 != 0 && mbm <= 1400) {
		printk(KERN_WARNING RFM9X_TAG
		       "tx power must be multiple of 20 mbm if <= 1400\n");
		return -EINVAL;
	} else if (mbm % 100 != 0) {
		printk(KERN_WARNING RFM9X_TAG
		       "tx power must be multiple of 100 mbm if > 1400\n");
		return -EINVAL;
	}

	// handle special case of max tx power
	max_power = 0;
	if (mbm == 2000) {
		pa_dac = 0x7;
		pa_select = 0x1;
		output_power = 0xf;
	}

	// regular, linear power range
	else if (mbm >= -400 && mbm <= 1700) {
		pa_dac = 0x4;

		if (mbm > 1400) {
			pa_select = 1;
			output_power = (mbm / 100) - 2;
		} else {
			pa_select = 0;
			// TODO: figure out best method to calculate values.
			return -1;
		}
	} else {
		printk(KERN_WARNING RFM9X_TAG
		       "requested tx power is out of range\n");
		return -ERANGE;
	}

	status = regmap_write(rfm9x->map, RFM9X_REG_PA_DAC, pa_dac);
	if (status < 0)
		return status;

	status = regmap_write(rfm9x->map, RFM9X_REG_PA_CONFIG,
			      (pa_select << 7) | (max_power << 4) |
				      output_power);
	if (status < 0)
		return status;

	return 0;
}

struct rfm9x_tx_data {
	struct rfm9x* rfm9x;
	void (*callback)(void *);
	void *param;
	struct spi_message message;
};

void rfm9x_free_tx_message(void *context)
{
	struct rfm9x_tx_data *data = context;
	data->callback(data->param);
	kfree(data);
}

/*
int rfm9x_transmit_async(struct rfm9x *rfm9x, struct sk_buff *skb,
			 void (*callback)(void *), void *param)
{
	struct rfm9x_tx_data *data;
	struct spi_message *message;

	// TODO cleanup module, get into tx mode

	data = kmalloc(sizeof(struct rfm9x_tx_data), GFP_KERNEL);
	message = &data->message;
	spi_message_init_with_transfers(message, NULL, 0);
	message->spi = rfm9x->spi;
	message->context = data;
	message->complete = rfm9x_free_tx_message;

	return spi_async(rfm9x->spi, message);
}
*/

// device management
static const struct of_device_id rfm9x_dt_ids[] = {
	{ .compatible = "hoperf,rfm9x" },
	{ .compatible = "hoperf,rfm95" },
	{ .compatible = "hoperf,rfm96" },
	{ .compatible = "hoperf,rfm97" },
	{ .compatible = "hoperf,rfm98w" },
	{ .compatible = "rfm9x" },
	{}
};

MODULE_DEVICE_TABLE(of, rfm9x_dt_ids);

bool rfm9x_writeable_reg(struct device *dev, unsigned int reg)
{
	if (reg > 0x11 && reg < 0x1d)
		return false;

	switch (reg) {
	case 0x10:
		return false;
	case 0x25:
		return false;
	}

	return true;
}

bool rfm9x_volatile_reg(struct device *dev, unsigned int reg)
{
	return true;
}

static struct regmap_config rfm9x_regmap_config = {
	.name = "lora",
	.reg_bits = 8,
	.val_bits = 8,
	.max_register = RFM9X_MAX_REG,
	.read_flag_mask = 0x00,
	.write_flag_mask = 0x80,
	.volatile_reg = rfm9x_volatile_reg,
	.writeable_reg = rfm9x_writeable_reg,
};

struct rfm9x *devm_rfm9x_init(struct spi_device *spi)
{
	int status;
	u8 version;
	struct rfm9x *rfm9x;

	rfm9x = devm_kmalloc(&spi->dev, sizeof(*rfm9x), GFP_KERNEL);
	if (rfm9x == NULL)
		return ERR_PTR(-ENOMEM);

	rfm9x->spi = spi;
	rfm9x->reset = devm_gpiod_get(&spi->dev, "reset", GPIOD_OUT_HIGH);
	if (IS_ERR(rfm9x->reset)) {
		printk(KERN_WARNING RFM9X_TAG "error getting reset gpio: %p\n",
		       rfm9x->reset);
		return (struct rfm9x *)rfm9x->reset;
	}

	rfm9x->map = devm_regmap_init_spi(spi, &rfm9x_regmap_config);
	if (rfm9x->map == NULL) {
		printk(KERN_WARNING RFM9X_TAG "error initializing regmap\n");
		return NULL;
	}

	spi_set_drvdata(spi, rfm9x);

	// reset module
	usleep_range(100, 1000);
	gpiod_set_value(rfm9x->reset, 0);
	usleep_range(10000, 100000);

	status = rfm9x_get_version(rfm9x, &version);
	if (status < 0)
		return ERR_PTR(status);

	// TODO: figure out error handling using version number
	/*
	if (version == 255) {
		printk(KERN_WARNING RFM9X_TAG
		       "error version is 255, likely not connected\n");
		return -1;
	}
    */

	printk(KERN_INFO RFM9X_TAG "device version is %u\n", version);
	printk(KERN_INFO RFM9X_TAG "initialized device\n");
	return rfm9x;
}

int rfm9x_remove(struct spi_device *spi)
{
	printk(KERN_INFO RFM9X_TAG "detached device\n");
	return 0;
}
