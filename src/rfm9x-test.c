// Test module for rfm9x
//
// Author: Matthew Knight
// File Name: rfm9x_test802154.c
// Date: 2019-12-11

#include "rfm9x.h"

#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/of.h>
#include <linux/regmap.h>
#include <linux/spi/spi.h>
#include <linux/spinlock.h>
#include <net/mac802154.h>

#define RFM9X_TEST_DEVICE_NAME "rfm9x_test"
#define RFM9X_TEST_TAG RFM9X_DEVICE_NAME ": "

#define RFM9X_TEST_MIN_FREQ 902000000
#define RFM9X_TEST_MAX_FREQ 928000000
#define RFM9X_TEST_CHAN_SPACE 600000
#define RFM9X_TEST_NUM_CHANNELS                                                \
	RFM9X_TEST_MAX_FREQ - RFM9X_TEST_MIN_FREQ -                            \
		RFM9X_TEST_CHAN_SPACE / RFM9X_TEST_CHAN_SPACE
#define RFM9X_TEST_MIN_CHAN RFM9X_TEST_MIN_FREQ + (RFM9X_TEST_CHAN_SPACE / 2)

struct rfm9x_test {
	struct rfm9x *rfm9x;
	struct mutex mode_mtx;
};

int rfm9x_test_get_chan_freq(u32 channel, u32 *freq)
{
	if (channel >= RFM9X_TEST_NUM_CHANNELS)
		return -1;

	*freq = RFM9X_TEST_MIN_CHAN + (RFM9X_TEST_CHAN_SPACE * channel);
	return 0;
}

static irq_handler_t tx_done_irq_handler(unsigned int irq, void *dev_id,
					 struct pt_regs *regs)
{
	printk(KERN_INFO RFM9X_TEST_TAG "tx done called\n");
	return (irq_handler_t)IRQ_HANDLED;
}

static irq_handler_t mode_change_irq_handler(unsigned int irq, void *dev_id,
					     struct pt_regs *regs)
{
	struct rfm9x_test *test;

	printk(KERN_INFO RFM9X_TEST_TAG "got mode change interrupt\n");
	test = dev_id;

	mutex_unlock(&test->mode_mtx);

	return (irq_handler_t)IRQ_HANDLED;
}

int rfm9x_test_wait_set_op_mode(struct rfm9x_test *test, enum op_mode mode)
{
	int status;

	status = mutex_lock_interruptible(&test->mode_mtx);
	if (status < 0)
		return status;

	status = rfm9x_set_op_mode(test->rfm9x, mode);
	if (status < 0)
		return status;

	status = mutex_lock_interruptible(&test->mode_mtx);
	if (status < 0)
		return status;

	mutex_unlock(&test->mode_mtx);
	return 0;
}

int rfm9x_test_tx_mode(struct rfm9x_test *test)
{
	int status, irq;

	irq = gpiod_to_irq(test->rfm9x->dio[0]);
	free_irq(irq, NULL);
	status = request_irq(irq, (irq_handler_t)tx_done_irq_handler,
			     IRQF_TRIGGER_RISING, "tx_done", test);
	if (status < 0)
		return status;

	status = rfm9x_test_wait_set_op_mode(test, rfm9x_rxcontinuous);
	if (status < 0)
		return status;

	return 0;
}

int rfm9x_test_set_channel(struct rfm9x_test *data, u32 channel)
{
	int status;
	u32 freq;

	status = rfm9x_test_get_chan_freq(channel, &freq);
	if (status < 0)
		return status;

	return rfm9x_set_carrier_freq(data->rfm9x, freq);
}

int rfm9x_test_start(struct rfm9x_test *data)
{
	return 0;
}

int rfm9x_test_probe(struct spi_device *spi)
{
	static const struct rfm9x_dio_config configs[] = {
		{ .dio = 0, .sig = rfm9x_dio_tx_done },
		{ .dio = 1, .sig = rfm9x_dio_rx_timeout },
		{ .dio = 3, .sig = rfm9x_dio_payload_crc_error },
		{ .dio = 4, .sig = rfm9x_dio_cad_detected },
		{ .dio = 5, .sig = rfm9x_dio_mode_ready },
	};

	int status, i;
	struct rfm9x *rfm9x;
	struct rfm9x_test *data;

	data = devm_kmalloc(&spi->dev, sizeof(struct rfm9x_test), GFP_KERNEL);
	// initialize bare minimum for rfm9x
	rfm9x = devm_rfm9x_init(spi);
	if (IS_ERR(rfm9x))
		return PTR_ERR(rfm9x);

	status = rfm9x_set_long_range_mode(rfm9x, lora);
	if (status < 0) {
		printk(KERN_WARNING RFM9X_TAG "error setting lora mode\n");
		return status;
	}

	// ensure that provided dio are configured correctly
	for (i = 0; i < RFM9X_NUM_DIO; ++i) {
		rfm9x->dio[i] =
			devm_gpiod_get_index(&spi->dev, "dio", i, GPIOD_IN);
		if (IS_ERR(rfm9x->dio[i])) {
			printk(KERN_WARNING RFM9X_TAG
			       "error registering dio %d",
			       i);
			return PTR_ERR(rfm9x->dio[i]);
		}

		status = rfm9x_configure_dio(rfm9x, &configs[i]);
		if (status < 0)
			return status;

		status = gpiod_to_irq(rfm9x->dio[i]);
		if (status < 0) {
			printk(KERN_WARNING RFM9X_TAG
			       "dio %d is not interruptible\n",
			       i);
			return status;
		}
	}

	// init mutexes and whatnot
	data->rfm9x = rfm9x;
	mutex_init(&data->mode_mtx);

	i = gpiod_to_irq(rfm9x->dio[0]);
	if (i < 0)
		return i;

	status = request_irq(i, (irq_handler_t)tx_done_irq_handler,
			     IRQF_TRIGGER_RISING, "tx_done_handler", data);
	if (status < 0)
		return status;

	i = gpiod_to_irq(rfm9x->dio[5]);
	if (i < 0)
		return i;

	status = request_irq(i, (irq_handler_t)mode_change_irq_handler,
			     IRQF_TRIGGER_RISING, "mode_change_handler", data);
	if (status < 0)
		return status;

	spi_set_drvdata(spi, data);

	return rfm9x_test_start(data);
}

int rfm9x_test_remove(struct spi_device *spi)
{
	return rfm9x_remove(spi);
}

// device management
static const struct of_device_id rfm9x_test_dt_ids[] = {
	{ .compatible = "semtech,rfm9x_test" },
	{}
};

MODULE_DEVICE_TABLE(of, rfm9x_test_dt_ids);

static struct spi_driver rfm9x_test_spi_driver = {
	.driver =
		{
			.name = RFM9X_DEVICE_NAME,
			.owner = THIS_MODULE,
			.of_match_table = of_match_ptr(rfm9x_test_dt_ids),
		},
	.probe = rfm9x_test_probe,
	.remove = rfm9x_test_remove,
};

module_spi_driver(rfm9x_test_spi_driver);

MODULE_AUTHOR("Matthew Knight <mgk1795@gmail.com>");
MODULE_DESCRIPTION("RFM95x Lora to 802.15.4 interface module");
MODULE_LICENSE("GPL");
