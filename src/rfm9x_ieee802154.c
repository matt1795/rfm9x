// 802.15.4 Interface Module for rfm9x
//
// Author: Matthew Knight
// File Name: rfm9x_ieee802154.c
// Date: 2019-12-11

#include "rfm9x.h"

#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/of.h>
#include <linux/regmap.h>
#include <linux/spi/spi.h>
#include <linux/spinlock.h>
#include <net/mac802154.h>

#define RFM9X_IEEE_DEVICE_NAME "rfm9x_ieee"
#define RFM9X_IEEE_TAG RFM9X_DEVICE_NAME ": "

#define RFM9X_IEEE_MIN_FREQ 902000000
#define RFM9X_IEEE_MAX_FREQ 928000000
#define RFM9X_IEEE_CHAN_SPACE 600000
#define RFM9X_IEEE_NUM_CHANNELS                                                \
	RFM9X_IEEE_MAX_FREQ - RFM9X_IEEE_MIN_FREQ -                            \
		RFM9X_IEEE_CHAN_SPACE / RFM9X_IEEE_CHAN_SPACE
#define RFM9X_IEEE_MIN_CHAN RFM9X_IEEE_MIN_FREQ + (RFM9X_IEEE_CHAN_SPACE / 2)

struct rfm9x_ieee {
	struct rfm9x *rfm9x;
	struct ieee802154_hw *hw;
	struct mutex mode_mtx;
};

static const s32 rfm9x_ieee_tx_powers[] = { -400, -300, -200, -100, 0,	  100,
					    200,  300,	400,  500,  600,  700,
					    800,  900,	1000, 1100, 1200, 1300,
					    1400, 1500, 1600, 1700, 2000 };

int rfm9x_ieee_get_chan_freq(u8 page, u8 channel, u32 *freq)
{
	u32 num = (page * 32) + channel;

	if (num >= RFM9X_IEEE_NUM_CHANNELS)
		return -1;

	*freq = RFM9X_IEEE_MIN_CHAN + (RFM9X_IEEE_CHAN_SPACE * num);
	return 0;
}

int rfm9x_ieee_set_channel_mask(u32 (*channels)[32])
{
	int i, pages_set, remainder;

	pages_set = RFM9X_IEEE_NUM_CHANNELS / 32;
	remainder = RFM9X_IEEE_NUM_CHANNELS % 32;

	for (i = 0; i < 32; ++i) {
		if (i < pages_set)
			*channels[i] = 0xffffffff;
		else if (i == pages_set)
			*channels[i] = 0xffffffff >> (32 - remainder);
		else
			*channels[i] = 0;
	}

	return 0;
}

static irq_handler_t tx_done_irq_handler(unsigned int irq, void *dev_id,
					 struct pt_regs *regs)
{
	printk(KERN_INFO RFM9X_IEEE_TAG "tx done called\n");
	return (irq_handler_t)IRQ_HANDLED;
}

static irq_handler_t rx_done_irq_handler(unsigned int irq, void *dev_id,
					 struct pt_regs *regs)
{
	printk(KERN_INFO RFM9X_IEEE_TAG "rx done called\n");
	return (irq_handler_t)IRQ_HANDLED;
}

static irq_handler_t mode_change_irq_handler(unsigned int irq, void *dev_id,
					     struct pt_regs *regs)
{
	struct rfm9x_ieee *ieee;

	printk(KERN_INFO RFM9X_IEEE_TAG "got mode change interrupt\n");
	ieee = dev_id;

	mutex_unlock(&ieee->mode_mtx);

	return (irq_handler_t)IRQ_HANDLED;
}

int rfm9x_ieee_wait_set_op_mode(struct rfm9x_ieee *ieee, enum op_mode mode)
{
	int status;

	status = mutex_lock_interruptible(&ieee->mode_mtx);
	if (status < 0)
		return status;

	status = rfm9x_set_op_mode(ieee->rfm9x, mode);
	if (status < 0)
		return status;

	status = mutex_lock_interruptible(&ieee->mode_mtx);
	if (status < 0)
		return status;

	mutex_unlock(&ieee->mode_mtx);
	return 0;
}

int rfm9x_ieee_tx_mode(struct rfm9x_ieee *ieee)
{
	int status, irq;

	irq = gpiod_to_irq(ieee->rfm9x->dio[0]);
	free_irq(irq, NULL);
	status = request_irq(irq, (irq_handler_t)tx_done_irq_handler,
			     IRQF_TRIGGER_RISING, "tx_done", ieee);
	if (status < 0)
		return status;

	status = rfm9x_ieee_wait_set_op_mode(ieee, rfm9x_rxcontinuous);
	if (status < 0)
		return status;

	return 0;
}

int rfm9x_ieee_rx_mode(struct rfm9x_ieee *ieee)
{
	int status, irq;

	irq = gpiod_to_irq(ieee->rfm9x->dio[0]);
	free_irq(irq, NULL);
	status = request_irq(irq, (irq_handler_t)rx_done_irq_handler,
			     IRQF_TRIGGER_RISING, "rx_done", ieee);
	if (status < 0)
		return status;

	status = rfm9x_ieee_wait_set_op_mode(ieee, rfm9x_rxcontinuous);
	if (status < 0)
		return status;

	return 0;
}

int rfm9x_ieee_start(struct ieee802154_hw *hw)
{
	int status, irq;
	struct rfm9x_ieee *ieee;

	printk(KERN_INFO RFM9X_IEEE_TAG "ieee_start called\n");
	ieee = hw->priv;
	irq = gpiod_to_irq(ieee->rfm9x->dio[5]);
	status = request_irq(irq, (irq_handler_t)mode_change_irq_handler,
			     IRQF_TRIGGER_RISING, "mode_change_irq", ieee);
	if (status < 0) {
		printk(KERN_WARNING RFM9X_IEEE_TAG "request irq: %d\n", status);
		return status;
	}

	status = rfm9x_ieee_wait_set_op_mode(ieee, rfm9x_rxcontinuous);
	if (status < 0) {
		printk(KERN_WARNING RFM9X_IEEE_TAG "wait set op: %d\n", status);
		return status;
	}

	// init irq state machine for handling packets

	return 0;
}

void rfm9x_ieee_stop(struct ieee802154_hw *hw)
{
	int status;
	struct rfm9x_ieee *ieee = hw->priv;

	// TODO: finish up transmit or close down rxcontinuous

	status = rfm9x_set_op_mode(ieee->rfm9x, rfm9x_sleep);
	if (status < 0)
		printk(KERN_WARNING RFM9X_IEEE_TAG
		       "failed to put device in sleep mode\n");
}

int rfm9x_ieee_xmit_async(struct ieee802154_hw *hw, struct sk_buff *skb)
{
	return -1;
}

// TODO: find out what the hell this is for
int rfm9x_ieee_ed(struct ieee802154_hw *hw, u8 *level)
{
	int status, rssi;
	s32 range;
	struct rfm9x_ieee *ieee;
	int sensitivity = 0;

	range = 0;
	ieee = hw->priv;
	status = rfm9x_get_rssi(ieee->rfm9x, &rssi);
	if (status < 0)
		return status;

	if (rssi < (sensitivity - 10))
		*level = 0;
	else if (rssi >= 0)
		*level = 255;
	else
		*level = (255 * (rssi + range) / range) % 255;

	return 0;
}

int rfm9x_ieee_set_channel(struct ieee802154_hw *hw, u8 page, u8 channel)
{
	int status;
	struct rfm9x_ieee *ieee;
	u32 freq;

	ieee = hw->priv;
	status = rfm9x_ieee_get_chan_freq(page, channel, &freq);
	if (status < 0)
		return status;

	return rfm9x_set_carrier_freq(ieee->rfm9x, freq);
}

int rfm9x_ieee_set_txpower(struct ieee802154_hw *hw, s32 mbm)
{
	struct rfm9x_ieee *ieee = hw->priv;
	printk(KERN_INFO RFM9X_IEEE_TAG "tx power set to %d mbm", mbm);
	return rfm9x_set_tx_power(ieee->rfm9x, mbm);
}

int rfm9x_ieee_set_promiscuous_mode(struct ieee802154_hw *hw, const bool on)
{
	return 0;
}

static struct ieee802154_ops rfm9x_ops = {
	.owner = THIS_MODULE,
	.start = rfm9x_ieee_start,
	.stop = rfm9x_ieee_stop,
	.xmit_async = rfm9x_ieee_xmit_async,
	.ed = rfm9x_ieee_ed,
	.set_channel = rfm9x_ieee_set_channel,
	.set_txpower = rfm9x_ieee_set_txpower,
	.set_promiscuous_mode = rfm9x_ieee_set_promiscuous_mode,
};

struct rfm9x_ieee *rfm9x_ieee_init(struct spi_device *spi, struct rfm9x *rfm9x)
{
	int status;
	struct ieee802154_hw *hw;
	struct rfm9x_ieee *ieee;

	hw = ieee802154_alloc_hw(sizeof(struct rfm9x_ieee), &rfm9x_ops);
	if (IS_ERR(hw))
		return ERR_PTR(-ENOMEM);

	ieee = hw->priv;
	ieee->hw = hw;
	ieee->rfm9x = rfm9x;
	hw->parent = &spi->dev;

	status = rfm9x_ieee_set_channel_mask(&hw->phy->supported.channels);
	if (status < 0)
		return ERR_PTR(status);

	hw->phy->current_channel = 0;
	hw->phy->supported.tx_powers = rfm9x_ieee_tx_powers;
	hw->phy->supported.tx_powers_size = ARRAY_SIZE(rfm9x_ieee_tx_powers);
	hw->phy->transmit_power = rfm9x_ieee_tx_powers[0];

	// TODO: what does this do?
	ieee802154_random_extended_addr(&hw->phy->perm_extended_addr);
	hw->flags = IEEE802154_HW_TX_OMIT_CKSUM | IEEE802154_HW_RX_OMIT_CKSUM |
		    IEEE802154_HW_PROMISCUOUS;

	// register hw
	status = ieee802154_register_hw(hw);
	if (status < 0)
		return ERR_PTR(status);

	// remember to init any locks/mutexes

	return 0;
}

void rfm9x_ieee_free(struct rfm9x_ieee *ieee)
{
	ieee802154_unregister_hw(ieee->hw);
	ieee802154_free_hw(ieee->hw);
}

int rfm9x_ieee_probe(struct spi_device *spi)
{
	static const struct rfm9x_dio_config configs[] = {
		{ .dio = 1, .sig = rfm9x_dio_rx_timeout },
		{ .dio = 3, .sig = rfm9x_dio_payload_crc_error },
		{ .dio = 4, .sig = rfm9x_dio_cad_detected },
		{ .dio = 5, .sig = rfm9x_dio_mode_ready },
	};

	int status, i;
	struct rfm9x *rfm9x;
	struct rfm9x_ieee *ieee;

	// initialize bare minimum for rfm9x
	rfm9x = devm_rfm9x_init(spi);
	if (IS_ERR(rfm9x))
		return PTR_ERR(rfm9x);

	// ensure that provided dio are configured correctly
	for (i = 0; i < RFM9X_NUM_DIO; ++i) {
		rfm9x->dio[i] =
			devm_gpiod_get_index(&spi->dev, "dio", i, GPIOD_IN);
		if (IS_ERR(rfm9x->dio[i])) {
			printk(KERN_WARNING RFM9X_TAG
			       "error registering dio %d",
			       i);
			return PTR_ERR(rfm9x->dio[i]);
		}

		status = gpiod_to_irq(rfm9x->dio[i]);
		if (status < 0) {
			printk(KERN_WARNING RFM9X_TAG
			       "dio %d is not interruptible\n",
			       i);
			return status;
		}
	}

	status = rfm9x_set_long_range_mode(rfm9x, lora);
	if (status < 0) {
		printk(KERN_WARNING RFM9X_TAG "error setting lora mode\n");
		return status;
	}

	// initialize DIO
	for (i = 0; i < ARRAY_SIZE(configs); ++i) {
		status = rfm9x_configure_dio(rfm9x, &configs[i]);
		if (status < 0)
			return status;
	}

	ieee = rfm9x_ieee_init(spi, rfm9x);
	if (IS_ERR(ieee))
		return PTR_ERR(ieee);

	// init mutexes and whatnot
	mutex_init(&ieee->mode_mtx);

	spi_set_drvdata(spi, ieee);
	return 0;
}

int rfm9x_ieee_remove(struct spi_device *spi)
{
	return rfm9x_remove(spi);
}

// device management
static const struct of_device_id rfm9x_ieee_dt_ids[] = {
	{ .compatible = "semtech,rfm9x_lora_802154" },
	{}
};

MODULE_DEVICE_TABLE(of, rfm9x_ieee_dt_ids);

static struct spi_driver rfm9x_ieee_spi_driver = {
	.driver =
		{
			.name = RFM9X_DEVICE_NAME,
			.owner = THIS_MODULE,
			.of_match_table = of_match_ptr(rfm9x_ieee_dt_ids),
		},
	.probe = rfm9x_ieee_probe,
	.remove = rfm9x_ieee_remove,
};

module_spi_driver(rfm9x_ieee_spi_driver);

MODULE_AUTHOR("Matthew Knight <mgk1795@gmail.com>");
MODULE_DESCRIPTION("RFM95x Lora to 802.15.4 interface module");
MODULE_LICENSE("GPL");
