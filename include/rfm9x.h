// RFM9X Transciever Module Interface
//
// Author: Matthew Knight
// File Name: rfm9x.h
// Date: 2019-12-10

#include <linux/module.h>
#include <linux/spi/spi.h>

#ifndef RRM9X_INTERFACE
#define RFM9X_INTERFACE

#define RFM9X_DEVICE_NAME "rfm9x"
#define RFM9X_TAG RFM9X_DEVICE_NAME ": "
#define RFM9X_NUM_DIO 6

// registers
#define RFM9X_REG_FIFO 0x00
#define RFM9X_REG_OP_MODE 0x01
#define RFM9X_REG_FR_MSB 0x06
#define RFM9X_REG_FR_MID 0x07
#define RFM9X_REG_FR_LSB 0x08
#define RFM9X_REG_PA_CONFIG 0x09
#define RFM9X_REG_PA_RAMP 0x0a
#define RFM9X_REG_OCP 0x0b
#define RFM9X_REG_LNA 0x0c
#define RFM9X_REG_FIFO_ADDR_PTR 0x0d
#define RFM9X_REG_FIFO_TX_BASE_ADDR 0x0e
#define RFM9X_REG_FIFO_RX_BASE_ADDR 0x0f
#define RFM9X_REG_FIFO_RX_CURRENT_ADDR 0x10
#define RFM9X_REG_IRQ_FLAGS_MASK 0x11
#define RFM9X_REG_IRQ_FLAGS 0x12
#define RFM9X_REG_NB_RX_BYTES 0x13
#define RFM9X_REG_RX_HEADER_CNT_VALUE_MSB 0x14
#define RFM9X_REG_RX_HEADER_CNT_VALUE_LSB 0x15
#define RFM9X_REG_RX_PACKET_CNT_VALUE_MSB 0x16
#define RFM9X_REG_RX_PACKET_CNT_VALUE_LSB 0x17
#define RFM9X_REG_MODEM_STAT 0x18
#define RFM9X_REG_PKT_SNR_VALUE 0x19
#define RFM9X_REG_RSSI_VALUE 0x1a
#define RFM9X_REG_PKT_RSSI_VALUE 0x1b
#define RFM9X_REG_HOP_CHANNEL 0x1c
#define RFM9X_REG_MODEM_CONFIG1 0x1d
#define RFM9X_REG_MODEM_CONFIG2 0x1e
#define RFM9X_REG_SYMB_TIMEOUT_LSB 0x1f
#define RFM9X_REG_PREAMBLE_MSB 0x20
#define RFM9X_REG_PREAMBLE_LSB 0x21
#define RFM9X_REG_PAYLOAD_LENGTH 0x22
#define RFM9X_REG_MAX_PAYLOAD_LENGTH 0x23
#define RFM9X_REG_HOP_PERIOD 0x24
#define RFM9X_REG_FIFO_RX_BYTE_ADDR 0x25
#define RFM9X_REG_MODEM_CONFIG3 0x26
#define RFM9X_REG_DIO_MAPPING1 0x40
#define RFM9X_REG_DIO_MAPPING2 0x41
#define RFM9X_REG_VERSION 0x42
#define RFM9X_REG_TXCO 0x4b
#define RFM9X_REG_PA_DAC 0x4d
#define RFM9X_REG_FORMER_TEMP 0x5b
#define RFM9X_REG_AGC_REF 0x61
#define RFM9X_REG_AGC_THRESH1 0x62
#define RFM9X_REG_AGC_THRESH2 0x63
#define RFM9X_REG_AGC_THRESH3 0x64
#define RFM9X_MAX_REG RFM9X_REG_AGC_THRESH3

// fields

// OpMode Register
#define RFM9X_FIELD_LONG_RANGE_MODE REG_FIELD(RFM9X_REG_OP_MODE, 7, 7)
#define RFM9X_FIELD_ACCESS_SHARED_REG REG_FIELD(RFM9X_REG_OP_MODE, 6, 6)
#define RFM9X_FIELD_LOW_FREQUENCY_MODE_ON REG_FIELD(RFM9X_REG_OP_MODE, 3, 3)
#define RFM9X_FIELD_MODE REG_FIELD(RFM9X_REG_OP_MODE, 2, 0)

// PaConfig Register
#define RFM9X_FIELD_PA_SELECT REG_FIELD(RFM9X_REG_PA_CONFIG, 7, 7)
#define RFM9X_FIELD_MAX_POWER REG_FIELD(RFM9X_REG_PA_CONFIG, 6, 4)
#define RFM9X_FIELD_OUTPUT_POWER REG_FIELD(RFM9X_REG_PA_CONFIG, 3, 0)

// Ocp Register
#define RFM9X_FIELD_OCP_ON REG_FIELD(RFM9X_REG_OCP, 5, 5)
#define RFM9X_FIELD_OCP_TRIM REG_FIELD(RFM9X_REG_OCP, 4, 0)

// Lna Register
#define RFM9X_FIELD_LNA_GAIN REG_FIELD(RFM9X_REG_LNA, 7, 5)
#define RFM9X_FIELD_LNA_BOOST_LF REG_FIELD(RFM9X_REG_LNA, 4, 3)
#define RFM9X_FIELD_LNA_BOOST_HF REG_FIELD(RFM9X_REG_LNA, 1, 0)

// IrqMask Register
#define RFM9X_FIELD_RX_TIMEOUT_MASK REG_FIELD(RFM9X_REG_IRQ_FLAGS_MASK, 7, 7)
#define RFM9X_FIELD_RX_DONE_MASK REG_FIELD(RFM9X_REG_IRQ_FLAGS_MASK, 6, 6)
#define RFM9X_FIELD_PAYLOAD_CRC_ERROR_MASK                                     \
	REG_FIELD(RFM9X_REG_IRQ_FLAGS_MASK, 5, 5)
#define RFM9X_FIELD_VALID_HEADER_MASK REG_FIELD(RFM9X_REG_IRQ_FLAGS_MASK, 4, 4)
#define RFM9X_FIELD_TX_DONE_MASK REG_FIELD(RFM9X_REG_IRQ_FLAGS_MASK, 3, 3)
#define RFM9X_FIELD_CAD_DONE_MASK REG_FIELD(RFM9X_REG_IRQ_FLAGS_MASK, 2, 2)
#define RFM9X_FIELD_FHSS_CHANGE_CHANNEL_MASK                                   \
	REG_FIELD(RFM9X_REG_IRQ_FLAGS_MASK, 1, 1)
#define RFM9X_FIELD_CAD_DETECTED_MASK REG_FIELD(RFM9X_REG_IRQ_FLAGS_MASK, 0, 0)

// IrqFlags Register
#define RFM9X_FIELD_RX_TIMEOUT REG_FIELD(RFM9X_REG_IRQ_FLAGS, 7, 7)
#define RFM9X_FIELD_RX_DONE REG_FIELD(RFM9X_REG_IRQ_FLAGS, 6, 6)
#define RFM9X_FIELD_PAYLOAD_CRC_ERROR REG_FIELD(RFM9X_REG_IRQ_FLAGS, 5, 5)
#define RFM9X_FIELD_VALID_HEADER REG_FIELD(RFM9X_REG_IRQ_FLAGS, 4, 4)
#define RFM9X_FIELD_TX_DONE REG_FIELD(RFM9X_REG_IRQ_FLAGS, 3, 3)
#define RFM9X_FIELD_CAD_DONE REG_FIELD(RFM9X_REG_IRQ_FLAGS, 2, 2)
#define RFM9X_FIELD_FHSS_CHANGE_CHANNEL REG_FIELD(RFM9X_REG_IRQ_FLAGS, 1, 1)
#define RFM9X_FIELD_CAD_DETECTED REG_FIELD(RFM9X_REG_IRQ_FLAGS, 0, 0)

// ModemStat Register
#define RFM9X_FIELD_RX_CODING_RATE REG_FIELD(RFM9X_REG_MODEM_STAT, 7, 5)
#define RFM9X_FIELD_MODEM_STATUS REG_FIELD(RFM9X_REG_MODEM_STAT, 4, 0)

// HopChannel Register
#define RFM9X_FIELD_PLL_TIMEOUT REG_FIELD(RFM9X_REG_HOP_CHANNEL, 7, 7)
#define RFM9X_FIELD_RX_PAYLOAD_CRC_ON_READ                                     \
	REG_FIELD(RFM9X_REG_HOP_CHANNEL, 6, 6)
#define RFM9X_FIELD_FHSS_PRESENT_CHANNEL REG_FIELD(RFM9X_REG_HOP_CHANNEL, 5, 0)

// ModemConfig1 Register
#define RFM9X_FIELD_BW REG_FIELD(RFM9X_REG_MODEM_CONFIG1, 7, 4)
#define RFM9X_FIELD_CODING_RATE REG_FIELD(RFM9X_REG_MODEM_CONFIG1, 3, 1)
#define RFM9X_FIELD_IMPLICIT_HEADER_MODE_ON                                    \
	REG_FIELD(RFM9X_REG_MODEM_CONFIG1, 0, 0)

// ModemConfig2 Register
#define RFM9X_FIELD_SPREADING_FACTOR REG_FIELD(RFM9X_REG_MODEM_CONFIG2, 7, 4)
#define RFM9X_FIELD_TX_CONTINUOUS_MODE REG_FIELD(RFM9X_REG_MODEM_CONFIG2, 3, 3)
#define RFM9X_FIELD_RX_PAYLOAD_CRC_ON_RW                                       \
	REG_FIELD(RFM9X_REG_MODEM_CONFIG2, 2, 2)
#define RFM9X_FIELD_SYMB_TIMEOUT REG_FIELD(RFM9X_REG_MODEM_CONFIG2, 1, 0)

// ModemConfig3 Register
#define RFM9X_FIELD_MOBILE_NODE REG_FIELD(RFM9X_REG_MODEM_CONFIG3, 3, 3)
#define RFM9X_FIELD_AGC_AUTO_ON REG_FIELD(RFM9X_REG_MODEM_CONFIG3, 2, 2)

// DioMapping Registers
#define RFM9X_FIELD_DIO0_MAPPING REG_FIELD(RFM9X_REG_DIO_MAPPING1, 7, 6)
#define RFM9X_FIELD_DIO1_MAPPING REG_FIELD(RFM9X_REG_DIO_MAPPING1, 5, 4)
#define RFM9X_FIELD_DIO2_MAPPING REG_FIELD(RFM9X_REG_DIO_MAPPING1, 3, 2)
#define RFM9X_FIELD_DIO3_MAPPING REG_FIELD(RFM9X_REG_DIO_MAPPING1, 1, 0)
#define RFM9X_FIELD_DIO4_MAPPING REG_FIELD(RFM9X_REG_DIO_MAPPING2, 7, 6)
#define RFM9X_FIELD_DIO5_MAPPING REG_FIELD(RFM9X_REG_DIO_MAPPING2, 5, 4)

// enums
enum op_mode {
	rfm9x_sleep = 0,
	rfm9x_stdby,
	rfm9x_fstx,
	rfm9x_tx,
	rfm9x_fsrx,
	rfm9x_rxcontinuous,
	rfm9x_rxsingle,
	rfm9x_cad,
};

enum long_range_mode {
	fsk = 0,
	lora,
};

enum coding_rate {
	four_five = 1,
	four_six,
	four_seven,
	four_eight,
};

enum rfm9x_irq {
	cad_detected = 0x01,
	fhss_change_channel = 0x02,
	cad_done = 0x04,
	tx_done = 0x08,
	valid_header = 0x10,
	payload_crc_error = 0x20,
	rx_done = 0x40,
	rx_timeout = 0x80,
};

enum rfm9x_dio_signal {
	rfm9x_dio_cad_detected,
	rfm9x_dio_cad_done,
	rfm9x_dio_clk_out,
	rfm9x_dio_fhss_change_channel,
	rfm9x_dio_mode_ready,
	rfm9x_dio_payload_crc_error,
	rfm9x_dio_pll_lock,
	rfm9x_dio_rx_done,
	rfm9x_dio_rx_timeout,
	rfm9x_dio_tx_done,
	rfm9x_dio_valid_header,
};

struct rfm9x {
	struct regmap *map;
	struct spi_device *spi;
	struct gpio_desc *reset;
	struct gpio_desc *dio[RFM9X_NUM_DIO];
};

struct rfm9x_dio_config {
	int dio;
	enum rfm9x_dio_signal sig;
};

/* Initialize bare minimum for rfm9x
 *
 * @param spi spi device to use.
 *
 * This init function also attaches the rfm9x struct to the spi device as
 * private data.
 */
struct rfm9x *devm_rfm9x_init(struct spi_device *spi);
int rfm9x_remove(struct spi_device *spi);
/*
int rfm9x_transmit_async(struct rfm9x *rfm9x, struct sk_buff *skb,
			 void (*callback)(void *), void *param);
			 */

int rfm9x_check_bitmask(struct rfm9x *rfm9x, unsigned int reg, u8 bitmask,
			bool *result);
int rfm9x_get_carrier_freq(struct rfm9x *rfm9x, unsigned int *freq);
int rfm9x_get_long_range_mode(struct rfm9x *rfm9x, enum long_range_mode *mode);
int rfm9x_get_packet_rssi(struct rfm9x *rfm9x, int *rssi);
int rfm9x_get_packet_snr(struct rfm9x *rfm9x, int *snr);
int rfm9x_get_packet_snr(struct rfm9x *rfm9x, int *snr);
int rfm9x_get_rssi(struct rfm9x *rfm9x, int *rssi);
int rfm9x_get_version(struct rfm9x *rfm9x, u8 *version);
int rfm9x_get_bandwidth(struct rfm9x *rfm9x, u32 *bandwidth);
int rfm9x_get_implicit_header_mode(struct rfm9x *rfm9x, bool *implicit_header);
int rfm9x_get_spreading_factor(struct rfm9x *rfm9x, u8 *sf);
int rfm9x_get_symb_timeout(struct rfm9x *rfm9x, u16 *timeout);
int rfm9x_get_preamble_length(struct rfm9x *rfm9x, u16 *length);
int rfm9x_get_tx_power(struct rfm9x *rfm9x, s32 *mbm);
int rfm9x_get_irq_flag(struct rfm9x *rfm9x, enum rfm9x_irq irq, bool *flag);

int rfm9x_set_carrier_freq(struct rfm9x *rfm9x, u32 freq);
int rfm9x_set_long_range_mode(struct rfm9x *rfm9x, enum long_range_mode mode);
int rfm9x_set_op_mode(struct rfm9x *rfm9x, enum op_mode mode);
int rfm9x_set_bandwidth(struct rfm9x *rfm9x, u32 bandwidth);
int rfm9x_set_implicit_header_mode(struct rfm9x *rfm9x, bool implicit_header);
int rfm9x_set_spreading_factor(struct rfm9x *rfm9x, u8 sf);
int rfm9x_set_symb_timeout(struct rfm9x *rfm9x, u16 timeout);
int rfm9x_set_preamble_length(struct rfm9x *rfm9x, u16 length);
int rfm9x_set_tx_power(struct rfm9x *rfm9x, s32 mbm);

int rfm9x_is_rx_ongoing(struct rfm9x *rfm9x, bool *ongoing);
int rfm9x_clear_irq_flag(struct rfm9x *rfm9x, enum rfm9x_irq irq);
int rfm9x_configure_dio(struct rfm9x *rfm9x,
			const struct rfm9x_dio_config *config);

#endif
