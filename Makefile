KERNELDIR=/lib/modules/$(shell uname -r)/build/
EXTRA_CFLAGS := -I$(src)/include

obj-m := rfm9x_lora_802154.o rfm9x_test.o
rfm9x_lora_802154-y := src/rfm9x.o src/rfm9x_ieee802154.o
rfm9x_test-y := src/rfm9x.o src/rfm9x-test.o

all:
	make -C $(KERNELDIR) M=$(PWD) modules
clean:
	make -C $(KERNELDIR) M=$(PWD) clean
